package com.qwertovsky.phone_book;

import static org.junit.Assert.*;

import org.junit.Test;

public class PhoneBookTest
{

	@Test
	public void testAddContact() throws Exception
	{
		PhoneBook phoneBook = new PhoneBook();
		phoneBook.addContact("Dima", "+79871234567");
		assertTrue("Can not add contact", phoneBook.isExists("Dima"));
	}
	
	@Test
	public void testAddExistedContact() throws Exception
	{
		PhoneBook phoneBook = new PhoneBook();
		phoneBook.addContact("Dima", "+79871234567");
		try
		{
			phoneBook.addContact("Dima", "+72343243243");
			fail("Added existed contact");
		}catch(Exception e)
		{
			// all right
		}
	}

	@Test
	public void testFindContact() throws Exception
	{
		PhoneBook phoneBook = new PhoneBook();
		phoneBook.addContact("Dima", "+79871234567");
		assertNotNull("Not found existed contact", phoneBook.findContact("Dima"));
	}
	
	@Test
	public void testDelContact() throws Exception
	{
		PhoneBook phoneBook = new PhoneBook();
		phoneBook.addContact("Dima", "+79871234567");
		phoneBook.delContact("Dima");
	}
	
	@Test
	public void testDelNotExistedContact()
	{
		PhoneBook phoneBook = new PhoneBook();
		try
		{
			phoneBook.delContact("Dima");
			fail("Deleted not existed contact");
		}catch(Exception e)
		{
			// all right
		}
	}

}
