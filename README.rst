Простенький  телефонный  справочник.

Интерфейс консольный.  Команды  три:  добавить  телефон  (слово) и имя (слово) в
справочник,  поискать  по  имени  телефон,  удалить  из справочника по
имени.

Добавлено сохранение в book.xml.

Команда для завершения и автоматического сохранения: QUIT.

Пример работы::

     > add Dima +7902525356
     'Dima' with phone '+7902525356' added

     > add Lena +791256322
     'Lena' with phone '+791256322' added

     > find Dima
     'Dima' with phone '+7902525356' found

     > add Dima +7902525356
     'Dima' already exists

     > find Masha
     'Masha' not found

     > del Dima
     'Dima' deleted
     
     > del Dima
     'Dima' not found

     > find Dima
     'Dima' not found

     > find Lena
     'Lena' with phone '+791256322' found
