package com.qwertovsky.phone_book;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="contacts")
public class PhoneBook
{
	@XmlElement(name = "contact")
	private List<Contact> contacts = new ArrayList<Contact>();
	
	// -------------------------------------------
	public boolean isExists(String name)
	{
		for(Contact contact:contacts)
		{
			String contactName = contact.getName();
			if(contactName.equals(name))
			{
				return true;
			}
		}
		return false;
	}
	
	// -------------------------------------------
	public void addContact(String name, String phone) throws Exception
	{
		if(isExists(name))
			throw new Exception("'"+ name + "' already exists");
		
		Contact contact = new Contact(name, phone);
		contacts.add(contact);
	}
	
	// -------------------------------------------
	public Contact findContact(String name)
	{
		for(Contact contact:contacts)
		{
			String contactName = contact.getName();
			if(contactName.equals(name))
			{
				return contact;
			}
		}
		return null;
	}
	
	// -------------------------------------------
	public void delContact(String name) throws Exception
	{
		if(!isExists(name))
			throw new Exception("'"+ name + "' not found");
		Contact contact = findContact(name);
		contacts.remove(contact);
	}
}
