package com.qwertovsky.phone_book;

import java.io.File;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Main
{
	private static PhoneBook phoneBook;
	private final static String BAD_COMMAND = "Bad command";
	private static Scanner input;
	
	private static File phoneBookFile = new File("book.xml");
	
	public static void main(String[] args)
	{
		System.out.println("Simple phone book");
		try
		{
			JAXBContext context = JAXBContext.newInstance(PhoneBook.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			phoneBook = (PhoneBook) unmarshaller.unmarshal(phoneBookFile);
		} catch (Exception e)
		{
			System.out.println("Error load phone book: " + e.getMessage());
			System.out.println("Created new book");
			phoneBook = new PhoneBook();
		} 
		
		input = new Scanner(System.in);
		while(true)
		{
			System.out.print("\n>");
			String command = input.nextLine();
			execute(command);
		}
		
	}
	
	// -------------------------------------------
	private static void execute(String command)
	{
		if(command == null || command.isEmpty())
		{
			System.out.println(BAD_COMMAND);
			return;
		}
		
		String[] words = command.split(" +");
		
		if(words.length == 0)
		{
			System.out.println(BAD_COMMAND);
			return;
		}
		
		command = words[0];
		
		if(command.equalsIgnoreCase("quit"))
		{
			if(words.length > 1)
			{
				System.out.println(BAD_COMMAND);
				return;
			}
			else 
				exit();
		}
		
		else if(command.equalsIgnoreCase("add"))
		{
			if(words.length != 3)
			{
				System.out.println(BAD_COMMAND);
			}
			else 
			{
				String name = words[1];
				String phone = words[2];
				try
				{
					phoneBook.addContact(name, phone);
					System.out.println("'" + name +"' with phone '" + phone + "' added");
				} catch (Exception e)
				{
					System.out.println(e.getMessage());
				}
			}
			return;
		}	
		
		else if(command.equalsIgnoreCase("find"))
		{
			if(words.length != 2)
			{
				System.out.println(BAD_COMMAND);
			}
			else 
			{
				String name = words[1];
				Contact contact = phoneBook.findContact(name);
				if(contact != null)
				{
					System.out.println("'" + contact.getName() + "' with phone '" + contact.getPhone() + "' found");
				}
				else
					System.out.println("'" + name + "' not found");
			}
			return;
		}
		
		else if(command.equalsIgnoreCase("del"))
		{
			if(words.length != 2)
			{
				System.out.println(BAD_COMMAND);
			}
			else 
			{
				String name = words[1];
				try
				{
					phoneBook.delContact(name);
					System.out.println("'" + name + "' deleted");
				} catch (Exception e)
				{
					System.out.println(e.getMessage());
				}
			}
			return;
		}
		
		else
		{
			System.out.println(BAD_COMMAND);
			return;
		}
		
	}
	
	// -------------------------------------------
	private static void exit()
	{
		// save phone book
		try
		{
			JAXBContext context = JAXBContext.newInstance(PhoneBook.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty("jaxb.formatted.output", true);
			marshaller.marshal(phoneBook, phoneBookFile);
		} catch (Exception e)
		{
			System.out.println("Error save phone book: " + e.getMessage());
			return;
		} 
		
		input.close();
		System.exit(0);
	}

}
